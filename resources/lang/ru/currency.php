<?php

return [
  'currencies_list_success_loaded' => 'Список валют получен',
  'number_of_results' => 'Количество:',
  'currencies_list_loading_failed' => 'Ошибка получения валют',
  'currency_rates_update_done' => 'Обновление курсов валют успешно завершено',
];
