<?php

return [
    'default_currency_code' => env('CURRENCY_DEFAULT_CURRENCY_CODE', 'USD'),
    'parser' => [
        'xe' => [
            'currency_list_selector' => '#__NEXT_DATA__',
            'method' => 'GET',
            'url' => 'https://www.xe.com/currencyconverter',
        ],
    ]
];
