<p align="center"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></p>

1. `cp .env.example .env`
2. Set up a database connection
3. Optional: Set CURRENCY_DEFAULT_CURRENCY_CODE to specify the default currency
4. `composer install`
5. `php artisan migrate`
6. Use insomnia dump
7. Well done :)
