<?php

namespace App\Providers;

use App\Contracts\ICurrencyConverterService;
use App\Contracts\ICurrencyParser;
use App\Services\Currency\Converters\CrossCourseConverter;
use App\Services\Currency\XeCurrencyParsingService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(ICurrencyParser::class, function () {
            return new XeCurrencyParsingService();
        });

        $this->app->singleton(ICurrencyConverterService::class, function () {
            return new CrossCourseConverter();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
