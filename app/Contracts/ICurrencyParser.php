<?php

namespace App\Contracts;

interface ICurrencyParser
{
    public function handleCurrencyList(): array;
    public function handleCurrencyExchangeRate(string $from, string $to): ?float;
}
