<?php

namespace App\Contracts;

use App\Models\Currency;

interface ICurrencyConverterService
{
    public function convertCurrency(Currency $currencyFrom, Currency $currencyTo, float $amount): float;
}
