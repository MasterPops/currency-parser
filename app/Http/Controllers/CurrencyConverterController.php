<?php

namespace App\Http\Controllers;

use App\Contracts\ICurrencyConverterService;
use App\Http\Requests\CurrencyConverterRequest;
use App\Models\Currency;

class CurrencyConverterController extends Controller
{
    public function __invoke(CurrencyConverterRequest $request, ICurrencyConverterService $currencyConverterService)
    {
        $currencyFrom = Currency::whereCode($request->get('from'))->firstOrFail();
        $currencyTo = Currency::whereCode($request->get('to'))->firstOrFail();
        $amount = $request->get('amount');

        $result = $currencyConverterService->convertCurrency($currencyFrom, $currencyTo, $amount);

        return response([
            'result' => $result,
        ]);
    }
}
