<?php

namespace App\Console\Commands;

use App\Contracts\ICurrencyParser;
use App\Models\Currency;
use Illuminate\Console\Command;

class UpdateCurrencyRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:currency-rates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updating all currency rates';

    protected ICurrencyParser $iCurrencyParser;

    public function __construct(ICurrencyParser $iCurrencyParser)
    {
        parent::__construct();

        $this->iCurrencyParser = $iCurrencyParser;
    }

    public function handle()
    {
        $this->updateCurrencies();
    }

    private function updateCurrencies()
    {
        $currencies = $this->iCurrencyParser->handleCurrencyList();

        if (!empty($currencies)) {
            $this->output->success(trans('currency.currencies_list_success_loaded')
                . "\n"
                . trans('currency.number_of_results')
                . count($currencies));

            $bar = $this->output->createProgressBar(count($currencies));
            $bar->start();

            foreach ($currencies as $currencyCode => $currencyData) {
                $currencyRate = $this->iCurrencyParser->handleCurrencyExchangeRate($currencyCode, config('currency.default_currency_code'));

                Currency::updateOrCreate([
                    'code' => $currencyCode,
                ], [
                    'name' => $currencyData['name'],
                    'rate' => $currencyRate,
                ]);

                $bar->advance();
            }

            $bar->finish();

            echo "\n";
            $this->output->success(trans('currency.currency_rates_update_done'));
        } else {
            $this->output->warning(trans('currency.currencies_list_loading_failed'));
        }
    }
}
