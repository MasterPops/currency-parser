<?php

namespace App\Services\Currency\Converters;

use App\Contracts\ICurrencyConverterService;
use App\Models\Currency;

class CrossCourseConverter implements ICurrencyConverterService
{
    public function convertCurrency(Currency $currencyFrom, Currency $currencyTo, float $amount): float
    {
        return round($amount * $currencyFrom->rate / $currencyTo->rate, 2);
    }
}
