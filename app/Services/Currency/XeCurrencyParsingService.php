<?php

namespace App\Services\Currency;

use App\Contracts\ICurrencyParser;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;

class XeCurrencyParsingService implements ICurrencyParser
{
    private Client $guzzleClient;

    public function __construct()
    {
        $this->guzzleClient = new Client();
    }

    public function handleCurrencyList(): array
    {
        $pageHtml = $this->handleHtml(config('currency.parser.xe.method'), config('currency.parser.xe.url'));

        if (!empty($pageHtml)) {
            $crawler = new Crawler($pageHtml);

            $json = $crawler->filter(config('currency.parser.xe.currency_list_selector'))->text();

            $currencyData = json_decode($json, true);

            return $currencyData['props']['pageProps']['commonI18nResources']['currencies']['en'] ?? [];
        }

        return [];
    }

    public function handleCurrencyExchangeRate(string $from, string $to, $amount = 1): ?float
    {
        $pageHtml = $this->handleHtml(config('currency.parser.xe.method'), config('currency.parser.xe.url') . "/convert/?Amount=$amount&From=$from&To=$to");

        if (!empty($pageHtml)) {
            $crawler = new Crawler($pageHtml);

            $currencyRateData = $crawler->filter('.result__BigRate-sc-1bsijpp-1')->text();

            $currencyRate = substr($currencyRateData, 0, strpos($currencyRateData, ' '));

            return (float)$currencyRate;
        }

        return null;
    }

    private function handleHtml(string $method, string $url): ?string
    {
        try {
            $response = $this->guzzleClient->request($method, $url);

            return (string)$response->getBody();
        } catch (GuzzleException $e) {
            $errorData = [
                'method' => $method,
                'url' => $url,
                'error' => $e->getMessage(),
            ];

            Log::channel('db')->warning('Failed request', $errorData);

            return null;
        }
    }
}
